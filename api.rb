require 'sinatra' 
require 'logger'
require 'json'
require 'rack/throttle'


logger = Logger.new(File.new("log.txt", "a+"))


def font_validation(font)
    dir = Dir.entries('./public/fonts')
    if dir.include? font 
        return true 
    else 
        return false 
    end 
end 

set :bind, '0.0.0.0' 
set :public_folder, 'public' 
set :protection, except: :frame_options
set :protection, except: :json_csrf

use Rack::Throttle::Daily, :max => 10000
use Rack::Throttle::Minute, :max => 100

configure do 
    enable :cross_origin 
    use Rack::CommonLogger, logger 
end  

# before do 
#     response.headers['Access-Control-Allow-Origin'] = '*'
# end 

# options "*" do
#     response.headers["Allow"] = "GET, PUT, POST, DELETE, OPTIONS"
#     response.headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type, Accept, X-User-Email, X-Auth-Token"
#     response.headers["Access-Control-Allow-Origin"] = "*"
#     200
# end

error 403 do 
    content_type :json
    {"error" => "Forbidden"}.to_json
end

error 404 do
    content_type :json
    {"error" => "Not Found"}.to_json
end


get '/' do 
    content_type :json
    {'message': 'Please use GET /fonts?font=FONTNAME to get your stylesheet'}.to_json
end 

get '/fonts' do 
    headers({'X-Frame-Options' => 'SAMEORIGIN', 'Timing-Allow-Origin' => '*'})
    cache_control :private, :max_age => 86400
    content_type "text/css"
    if params.has_key?(:font)
        if font_validation(params[:font])
            f = File.open("public/stylesheets/#{params[:font]}.css") 
            f.read 
        else 
            404 
        end 
    else 
        403 
    end 
end 

get '/list' do 
    headers({'X-Frame-Options' => 'SAMEORIGIN', 'Timing-Allow-Origin' => '*'})
    content_type :json 

    dir = Dir.entries('./public/stylesheets') 
    dir = dir.sort 
    dir.delete(".")
    dir.delete("..")
    
    font_names = []
    dir.each do |font|
        font_name = font.split(".")[0]
        font_families = []
        font_weights = []
        f = File.open("public/stylesheets/#{font}")
        f = f.readlines 
        f.each do |line|
            if line.include?("font-family:")
                font_families << line.split("font-family:")[1].split(";")[0].strip
            end
            if line.include?("font-weight:")
                font_weights << line.split("font-weight:")[1].split(";")[0].strip
            end
        end

        font_names << {"label" => font_name, "families" => font_families, "weights" => font_weights}
    end

    {"fonts_available" => font_names}.to_json 
end

