# Free Persian Fonts API 

Just a simple API for Free Perian Fonts 

## Current Fonts 

|Font Name | Creator           | 
|:---------:     |:-----------------:|
| Vazir          | Saber Rastikerdar | 
| Tanha          | Saber Rastikerdar |
| Vazir Code     | Saber Rastikerdar |
| Shabnam        | Saber Rastikerdar | 
| Samim          | Saber Rastikerdar | 
| Sahel          | Saber Rastikerdar | 
| Parastoo       | Saber Rastikerdar |
| Nahid          | Saber Rastikerdar |
| Gandom         | Saber Rastikerdar |
| Estedad        | Amin Abedi        |
| Mikhak         | Amin Abedi        |
| XB Kayhan      | -                 |
| XB Khoramshahr | -                 |
| XB Niloofar    | -                 |
| XB Roya        | -                 |
| XB Titre       | -                 |
| XB Zar         | -                 |

The table above is inaccurate. Please visit [this page](https://azadqalam.ir/fonts/) to see the complete list of fonts. 

## Test 
For using tests, clone this repo and run : 

```bundle install``` 

to install required ruby gems. Then, you easily can do a bunch of test like this: 

``` ruby cmd-test.rb Vazir ```

And if it's valid, it'll return the CSS it should return. Otherwise, it'll return a 403 error. 

## TO-DO 

- [ ] Writing a system that automatically reads data from font-stores and developer's websites (suggested by Alireza Fereydouni)
- [x] Applying request limit to improve the server uptime/downtime (Database is needed for this, maybe a token based database) - Done without DB. 
- [x] Doing a bunch of minor changes on code base to improve performance and security. 
- [x] Checking if gitlab has banned me or not :|
- [ ] Automatic CSS generator (for far future)

